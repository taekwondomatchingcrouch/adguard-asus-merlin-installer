#!/bin/sh

# AdGuard Home Installation Script
#
# 1. Download the package
# 2. Unpack it
# 3. Install as a service
#
# Requirements:
# . bash
# . which
# . printf
# . uname
# . id
# . head, tail
# . curl
# . tar or unzip
# . rm

HTTP_IP=192.168.1.1
HTTP_PORT=8090
PIDFILE=/var/run/adguardhome.pid
OUT_DIR=/opt
AGH_DIR="${OUT_DIR}/AdGuardHome"
CONFF="${AGH_DIR}/config.yaml"
DNS_IP=127.0.0.1
DNS_PORT=5483

init_conf()
{
cat << EOF
bind_host: $HTTP_IP
bind_port: $HTTP_PORT
dns:
  bind_host: $DNS_IP
  port: $DNS_PORT
  bootstrap_dns:
  - 9.9.9.10
  - 9.9.9.9
  - 2620:fe::10
  - 2620:fe::fe:10
  upstream_dns:
  - https://dns10.quad9.net/dns-query
  - https://dns.cloudflare.com/dns-query
  allowed_clients:
  - $DNS_IP
schema_version: 7

EOF
}

monit_t()
{
cat <<EOF
check process adguard with pidfile "$PIDFILE"
    start program = "$OUT_DIR/etc/init.d/S99adguardhome start"
    stop program = "$OUT_DIR/etc/init.d/S99adguardhome stop"
    if does not exist then restart
EOF
}

service_t()
{
cat << EOF
#!/bin/sh
source /usr/sbin/helper.sh

ENABLED=yes
PROCS=AdGuardHome
AGH_DIR=$AGH_DIR
dnsmasq_str="pc_replace \"servers-file=/tmp/resolv.dnsmasq\" \"servers-file=/opt/tmp/resolv.dnsmasq\" \\\$1"
HTTP_PORT=$HTTP_PORT
PIDFILE=$PIDFILE
HTTP_IP=$HTTP_IP
DNS_IP=$DNS_IP
DNS_PORT=$DNS_PORT
ARGS="-p \$HTTP_PORT -h \$HTTP_IP --config $CONFF -l /opt/var/log/AdGuardHome.log -w \$AGH_DIR --pidfile \$PIDFILE --no-check-update"
PREARGS=""
DESC=\$PROCS
PATH=$AGH_DIR:/opt/sbin:/opt/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

if [ "\$1" = start ]
then

        /bin/echo "server=\$DNS_IP#\$DNS_PORT" |tee /opt/tmp/resolv.dnsmasq > /dev/null
        pc_append "\$dnsmasq_str" /jffs/scripts/dnsmasq.postconf
        /sbin/service restart_dnsmasq

fi

if [ "\$1" = stop ]
then
         pc_delete "\$dnsmasq_str" /jffs/scripts/dnsmasq.postconf
         /sbin/service restart_dnsmasq
fi

. /opt/etc/init.d/rc.func

EOF

}


log_info()
{
    printf "[info] %s\\n" "$1"
}

log_error()
{
    printf "[error] %s\\n" "$1"
}

# Get OS
# Return: darwin, linux, freebsd
detect_os()
{
	UNAME_S="$(uname -s)"
	OS=
	case "$UNAME_S" in
		Linux)
			OS=linux
			;;

		FreeBSD)
			OS=freebsd
			;;

		Darwin)
			OS=darwin
			;;

		*)
			return 1
			;;

	esac

	echo $OS
}

# Get CPU endianness
# Return: le, ""
cpu_little_endian()
{
	ENDIAN_FLAG="$(head -c 6 /bin/sh | tail -c 1)"
	if [ "$ENDIAN_FLAG" = "$(printf '\001')" ]; then
		echo 'le'
		return 0
	fi
}

# Get CPU
# Return: amd64, 386, armv5, armv6, armv7, arm64, mips_softfloat, mipsle_softfloat, mips64_softfloat, mips64le_softfloat
detect_cpu()
{
	UNAME_M="$(uname -m)"
	CPU=

	case "$UNAME_M" in

		x86_64 | x86-64 | x64 | amd64)
			CPU=amd64
			;;

		i386 | i486 | i686 | i786 | x86)
			CPU=386
			;;

		armv5l | armv6l | armv7l | armv8l)
			CPU=armv5
			;;

		aarch64)
			CPU=arm64
			;;

		mips)
			LE=$(cpu_little_endian)
			CPU=mips${LE}_softfloat
			;;

		mips64)
			LE=$(cpu_little_endian)
			CPU=mips64${LE}_softfloat
			;;

		*)
			return 1

	esac

	echo "${CPU}"
}

# Get package file name extension
# Return: tar.gz, zip
package_extension()
{
	if [ "$OS" = "darwin" ]; then
		echo "zip"
		return 0
	fi
	echo "tar.gz"
}

# Download data to a file
# Use: download URL OUTPUT
download()
{
	log_info "Downloading package from $1 -> $2"
		curl -s "$1" --output "$2" || error_exit "Failed to download $1"
}

# Unpack package to a directory
# Use: unpack INPUT OUTPUT_DIR PKG_EXT
unpack()
{
	log_info "Unpacking package from $1 -> $2"
	mkdir -p "$2"
	if [ "$3" = "zip" ]; then
		unzip -qq "$1" -d "$2" || return 1
	elif [ "$3" = "tar.gz" ]; then
		tar xzf "$1" -C "$2" || return 1
	else
		return 1
	fi
}

# Print error message and exit
# Use: error_exit MESSAGE
error_exit()
{
	log_error "$1"
	exit 1
}

# Check if command exists
# Use: is_command COMMAND
is_command() {
    check_command="$1"
    type "${check_command}" >/dev/null 2>&1
}


# Entry point
main() {
    log_info "Starting AdGuard Home installation script"

    CHANNEL=${1}
    if [ "${CHANNEL}" != "beta" ] && [ "${CHANNEL}" != "edge" ]; then
        CHANNEL=release
    fi
    log_info "Channel ${CHANNEL}"

    OS=$(detect_os) || error_exit "Cannot detect your OS"
    CPU=$(detect_cpu) || error_exit "Cannot detect your CPU"
    PKG_EXT=$(package_extension)
    PKG_NAME=AdGuardHome_${OS}_${CPU}.${PKG_EXT}

    SCRIPT_URL="https://raw.githubusercontent.com/AdguardTeam/AdGuardHome/master/scripts/install.sh"
    URL="https://static.adguard.com/adguardhome/${CHANNEL}/${PKG_NAME}"
    OUT_DIR=/opt
    AGH_DIR="${OUT_DIR}/AdGuardHome"

    log_info "AdGuard Home will be installed to ${AGH_DIR}"

    download "${URL}" "${PKG_NAME}" || error_exit "Cannot download the package"

    [ -f ${OUT_DIR}/etc/init.d/S99adguardhome ] && ${OUT_DIR}/etc/init.d/S99adguardhome stop     
	unpack "${PKG_NAME}" "${OUT_DIR}" "${PKG_EXT}" || error_exit "Cannot unpack the package"

    # Install AdGuard Home service and run it
    rm "${PKG_NAME}"

    [ -f ${CONFF} ] || init_conf |tee ${CONFF} > /dev/null
    [ -f ${OUT_DIR}/etc/init.d/S99adguardhome ] || service_t |tee ${OUT_DIR}/etc/init.d/S99adguardhome > /dev/null
    [ -d ${OUT_DIR}/etc/monit.d ] && monit_t | tee ${OUT_DIR}/etc/monit.d/adguardhome.monit > /dev/null 
    type monit > /dev/null && monit reload
    
    chmod +x ${OUT_DIR}/etc/init.d/S99adguardhome
    ${OUT_DIR}/etc/init.d/S99adguardhome start     
    /opt/bin/entware-services check
    log_info "AdGuard Home is now installed and running."
    log_info "Access AdGuard Home from the web browser with this address http://$HTTP_IP ."
    log_info "You can control the service status with the following commands:"
    log_info "${OUT_DIR}/etc/init.d/S99adguardhome start|stop"
}

main "$@"
